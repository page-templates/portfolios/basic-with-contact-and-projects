document.addEventListener('DOMContentLoaded', (event) => {
    // Supposing you have an array of images for each project
    updateMedia(0);
    updateMedia(1);
});

let projects = [
    {
        name: "Project 2",
        description: "Project 2 description. The skills exercised.",
        media: [{ type: "video", link: "https://v.redd.it/g8m9xnw1ouk61/HLSPlaylist.m3u8", caption: "Demo" }],
        currentImageIndex: 0
    },
    {
        name: "Project 1",
        description: "Project 1 description. The skills exercised.",
        media: [{ type: "image", link: "https://cdn.hackaday.io/images/1555451682435877099.jpg", caption: "Fotos Mini" }, { type: "image", link: "https://cdn.hackaday.io/images/3015921680271693912.jpg", caption: "Board" }],
        currentImageIndex: 0
    }
];


function setMediaIndicator(projectIndex) {
    let project = projects[projectIndex];
    let mediaIndicator = document.getElementById(`mediaIndicator${projectIndex}`);
    mediaIndicator.textContent = (project.currentImageIndex + 1) + ' / ' + project.media.length;
}

function showPrevMedia(projectIndex) {
    let project = projects[projectIndex];
    if (project.currentImageIndex > 0) {
        project.currentImageIndex--;
    } else {
        project.currentImageIndex = project.media.length - 1;
    }
    updateMedia(projectIndex);
}

function showNextMedia(projectIndex) {
    let project = projects[projectIndex];
    if (project.currentImageIndex < project.media.length - 1) {
        project.currentImageIndex++;
    } else {
        project.currentImageIndex = 0;
    }
    updateMedia(projectIndex);
}

function updateMedia(projectIndex) {
    let project = projects[projectIndex];
    let media = project.media[project.currentImageIndex];
    let imgElement = document.getElementById(`projectImage${projectIndex}`);
    let videoElement = document.getElementById(`projectVideo${projectIndex}`);
    let captionElement = document.getElementById(`mediaCaption${projectIndex}`);

    captionElement.textContent = media.caption;  // assuming media object has a caption property

    if (media.type === 'image') {
        imgElement.src = media.link;
        imgElement.style.display = 'block';
        videoElement.style.display = 'none';
    } else if (media.type === 'video') {
        videoElement.querySelector('source').src = media.link;
        videoElement.load();  // Required to update the video source
        videoElement.style.display = 'block';
        imgElement.style.display = 'none';
    }

    let indicatorElement = document.getElementById(`mediaIndicator${projectIndex}`);
    indicatorElement.textContent = `${project.currentImageIndex + 1} / ${project.media.length}`;
}
